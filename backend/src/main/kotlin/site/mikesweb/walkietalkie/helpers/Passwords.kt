package site.mikesweb.walkietalkie.helpers

import org.mindrot.jbcrypt.BCrypt

fun String.toNewHash(): String {
    return BCrypt.hashpw(this, BCrypt.gensalt())
}

fun String.checkHashMatches(hash: String): Boolean {
    return BCrypt.checkpw(this, hash)
}