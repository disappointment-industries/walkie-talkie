package site.mikesweb.walkietalkie

import com.typesafe.config.ConfigFactory
import io.ktor.config.*
import io.ktor.server.cio.*
import io.ktor.server.engine.*
import site.mikesweb.walkietalkie.data.createSchema
import site.mikesweb.walkietalkie.web.*

fun main(args: Array<String>) {
    embeddedServer(CIO, environment = applicationEngineEnvironment {
        config = HoconApplicationConfig(ConfigFactory.load())

        module {
            createSchema()

            configureRouting()
            configureSecurity()
            configureSerialization()
            configureSockets()

            setupLogin()
            setupChannels()
            setupTurnKeygen()
        }

        connector {
            port = config.property("ktor.port").getString().toInt()
            developmentMode = config.property("ktor.development").getString().toBoolean()
        }
    }).start(wait = true)
}
