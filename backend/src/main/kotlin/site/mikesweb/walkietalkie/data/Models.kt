package site.mikesweb.walkietalkie.data

import org.jetbrains.exposed.dao.UUIDEntity
import org.jetbrains.exposed.dao.UUIDEntityClass
import org.jetbrains.exposed.dao.id.EntityID
import org.jetbrains.exposed.dao.id.UUIDTable
import java.util.*

object Users: UUIDTable() {
    val name = varchar("name", 50).uniqueIndex()
    val pwHash = char("pw_hash", 60)
}

class User(id: EntityID<UUID>): UUIDEntity(id) {
    companion object: UUIDEntityClass<User>(Users)

    var name by Users.name
    var pwHash by Users.pwHash
    var channels by Channel via ChannelMemberships
    val memberships by ChannelMembership referrersOn ChannelMemberships.user

    fun toMap() = mapOf(
        "name" to name,
        "id" to id.value.toString(),
    )
}

object ChannelMemberships: UUIDTable() {
    val channel = reference("channel", Channels).index()
    val user = reference("user", Users).index()
}

class ChannelMembership(id: EntityID<UUID>): UUIDEntity(id) {
    companion object: UUIDEntityClass<ChannelMembership>(ChannelMemberships)

    var user by User referencedOn ChannelMemberships.user
    var channel by Channel referencedOn ChannelMemberships.channel
    //val connections by Connection referrersOn Connections.channelMembership
}

object Channels: UUIDTable() {
    val name = varchar("name", 50).uniqueIndex()
}

class Channel(id: EntityID<UUID>): UUIDEntity(id) {
    companion object: UUIDEntityClass<Channel>(Channels)

    var name by Channels.name
    var users by User via ChannelMemberships
    val memberships by ChannelMembership referrersOn ChannelMemberships.channel
}

/*
object Connections: UUIDTable() {
    val ipaddress = varchar("ipaddress", 39)
    val port = integer("port")
    val channelMembership = reference("channelMembership", ChannelMemberships).index()
}

class Connection(id: EntityID<UUID>): UUIDEntity(id) {
    companion object: UUIDEntityClass<Connection>(Connections)

    var ipaddress by Connections.ipaddress
    var port by Connections.port
    var channelMembership by ChannelMembership referencedOn Connections.channelMembership
}
*/
