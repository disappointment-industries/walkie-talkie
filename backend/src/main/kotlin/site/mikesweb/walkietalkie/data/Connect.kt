package site.mikesweb.walkietalkie.data

import io.ktor.application.*
import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.SchemaUtils
import org.jetbrains.exposed.sql.transactions.transaction
import java.net.URI

private var cachedDb: Database? = null

val Application.db: Database
get() {
    if (cachedDb != null) return cachedDb!!

    val uri = URI(environment.config.property("db.uri").getString())

    val (user, password) = uri.userInfo.split(":")

    val query = if (uri.rawQuery == null) { "" } else { "?${uri.rawQuery}" }

    val url = "jdbc:${uri.scheme}://${uri.host}:${uri.port}${uri.path}${query}"

    cachedDb = Database.connect(
        url,
        driver = "org.postgresql.Driver",
        user = user,
        password = password,
    )

    return cachedDb!!
}

fun Application.createSchema() {
    db
    transaction {
        SchemaUtils.create(Users)
        SchemaUtils.create(Channels)
        SchemaUtils.create(ChannelMemberships)
        //SchemaUtils.create(Connections)
    }
}