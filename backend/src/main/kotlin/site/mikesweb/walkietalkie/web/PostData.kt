package site.mikesweb.walkietalkie.web

import kotlinx.serialization.Serializable

@Serializable
data class LoginData(
    val username: String,
    val password: String,
)

@Serializable
data class InChannel(
    val name: String,
)