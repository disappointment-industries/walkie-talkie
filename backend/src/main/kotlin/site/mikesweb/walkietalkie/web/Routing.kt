package site.mikesweb.walkietalkie.web

import com.auth0.jwt.JWT
import com.auth0.jwt.algorithms.Algorithm
import io.ktor.routing.*
import io.ktor.http.*
import io.ktor.features.*
import io.ktor.application.*
import io.ktor.auth.*
import io.ktor.auth.jwt.*
import io.ktor.request.*
import io.ktor.response.*
import io.ktor.util.Identity.decode
import kotlinx.serialization.ExperimentalSerializationApi
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.descriptors.SerialDescriptor
import kotlinx.serialization.encodeToString
import kotlinx.serialization.encoding.CompositeDecoder
import kotlinx.serialization.encoding.Decoder
import kotlinx.serialization.json.Json
import kotlinx.serialization.modules.SerializersModule
import org.jetbrains.exposed.sql.transactions.transaction
import site.mikesweb.walkietalkie.data.User
import site.mikesweb.walkietalkie.data.Users
import site.mikesweb.walkietalkie.data.db
import site.mikesweb.walkietalkie.helpers.checkHashMatches
import site.mikesweb.walkietalkie.helpers.toNewHash
import site.mikesweb.walkietalkie.web.LoginData
import java.util.*

fun Application.configureRouting() {
    routing {
        get("/") {
            call.respondRedirect("https://play.google.com/store/apps/details?id=site.mikesweb.walkietalkie")
        }
        install(StatusPages) {
            exception<AuthenticationException> { cause ->
                call.respond(HttpStatusCode.Unauthorized)
            }
            exception<AuthorizationException> { cause ->
                call.respond(HttpStatusCode.Forbidden)
            }
        }
    }
}

class AuthenticationException : RuntimeException()
class AuthorizationException : RuntimeException()
