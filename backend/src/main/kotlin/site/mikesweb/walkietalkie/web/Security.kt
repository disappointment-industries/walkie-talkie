package site.mikesweb.walkietalkie.web

import io.ktor.auth.*
import io.ktor.auth.jwt.*
import com.auth0.jwt.JWT
import com.auth0.jwt.algorithms.Algorithm
import io.ktor.application.*

val Application.jwtIssuer
get() = environment.config.property("jwt.issuer").getString()
val Application.jwtAudience
get() = environment.config.property("jwt.audience").getString()
val Application.jwtSecret
get() = environment.config.property("jwt.secret").getString()

fun Application.configureSecurity() {

    authentication {
        jwt {
            realm = environment.config.property("jwt.realm").getString()

            verifier(
                JWT
                    .require(Algorithm.HMAC256(jwtSecret))
                    .withAudience(jwtAudience)
                    .withIssuer(jwtIssuer)
                    .build()
            )
            validate { credential ->
                if (credential.payload.audience.contains(jwtAudience)) JWTPrincipal(credential.payload) else null
            }
        }
    }

}
