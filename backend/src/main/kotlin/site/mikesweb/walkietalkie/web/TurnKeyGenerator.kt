package site.mikesweb.walkietalkie.web

import io.ktor.application.*
import io.ktor.auth.*
import io.ktor.http.*
import io.ktor.response.*
import io.ktor.routing.*
import site.mikesweb.walkietalkie.data.db
import java.util.*
import javax.crypto.Mac
import javax.crypto.spec.SecretKeySpec
import kotlinx.serialization.Serializable


// This implements the REST API for TURN server authentication
// see: https://datatracker.ietf.org/doc/html/draft-uberti-behave-turn-rest-00
fun Application.setupTurnKeygen() {
    routing {
        authenticate {
            get("/turn") {
                val user = getUser(db)
                if (user == null) {
                    call.respond(HttpStatusCode.Unauthorized)
                    return@get
                }

                val secret = environment.config.property("turn.secret").getString()
                val server = environment.config.property("turn.server").getString()
                val sha256Hmac = Mac.getInstance("HmacSHA1")
                val secretKey = SecretKeySpec(secret.toByteArray(), "HmacSHA1")
                sha256Hmac.init(secretKey)

                val expire = System.currentTimeMillis() + 1000 * 60 * 10
                val username = "${expire}:${user.id}"
                val pass = Base64.getEncoder().encodeToString(sha256Hmac.doFinal(username.toByteArray()))

                call.respond(
                    TurnResponse(
                        username = username,
                        password = pass,
                        server = server,
                        expire = expire,
                    )
                )
            }
        }
    }
}

@Serializable
data class TurnResponse(
    val username: String,
    val password: String,
    val server: String,
    val expire: Long,
)