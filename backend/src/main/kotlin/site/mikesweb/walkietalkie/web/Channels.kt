package site.mikesweb.walkietalkie.web

import io.ktor.application.*
import io.ktor.auth.*
import io.ktor.auth.jwt.*
import io.ktor.http.*
import io.ktor.response.*
import io.ktor.routing.*
import kotlinx.coroutines.delay
import kotlin.text.get
import kotlinx.serialization.Serializable
import org.jetbrains.exposed.dao.with
import org.jetbrains.exposed.exceptions.ExposedSQLException
import org.jetbrains.exposed.sql.SizedCollection
import org.jetbrains.exposed.sql.SqlExpressionBuilder.eq
import org.jetbrains.exposed.sql.and
import org.jetbrains.exposed.sql.select
import org.jetbrains.exposed.sql.transactions.transaction
import site.mikesweb.walkietalkie.data.*
import java.util.*


fun Application.setupChannels() {
    routing {
        authenticate {
            get("/channels") {
                val start = call.request.queryParameters["startFrom"]?.toLong() ?: 0L
                val count = call.request.queryParameters["count"]?.toInt() ?: 100

                var resp: List<RespChannel>? = null
                getAll(db, Channel.Companion, count, start) {
                    resp = it.with(Channel::users).map { ch -> ch.respChannel }
                }
                call.respond(resp!!)
            }
            get("/channels/{id}") {
                var resp: RespChannel? = null
                getById(db, Channel.Companion) {
                    resp = it?.respChannel
                }
                if (resp == null) {
                    call.respond(HttpStatusCode.NotFound)
                } else {
                    call.respond(resp!!)
                }
            }
            post<InChannel>("/channels") {
                db
                lateinit var resp: RespChannel
                val user = getUser(db)
                try {
                    var ch: Channel? = null
                    transaction {
                        ch = Channel.new {
                            name = it.name
                        }
                        resp = ch!!.respChannel
                    }
                    if (user != null) {
                        transaction {
                            ch!!.users = SizedCollection(listOf(user))
                            resp = ch!!.respChannel
                        }
                    }
                } catch (e: ExposedSQLException) {
                    call.respond(HttpStatusCode.Conflict)
                }
                call.respond(resp)
            }
            post("/channels/{id}/join") {
                val user = getUser(db)!!
                var resp = HttpStatusCode.OK
                getById(db, Channel.Companion) {
                    if (it == null) {
                        resp = HttpStatusCode.NotFound
                        return@getById
                    }

                    val users = it.users.toMutableList()
                    users += user
                    it.users = SizedCollection(users)
                }
                call.respond(resp)
            }
            post("/channels/{id}/leave") {
                val user = getUser(db)!!
                getById(db, Channel.Companion) { channel ->
                    if (channel == null) {
                        return@getById
                    }

                    val query = ChannelMemberships
                        .innerJoin(Channels)
                        .innerJoin(Users)
                        .slice(ChannelMemberships.columns)
                        .select{
                            Channels.id eq channel.id and
                                    (Users.id eq user.id)
                        }.withDistinct()
                    val membership = ChannelMembership.wrapRow(query.first())
                    membership.delete()

                }
                call.respond(HttpStatusCode.OK)
            }
            delete("/channels/{id}") {
                val user = getUser(db)!!
                var resp = HttpStatusCode.OK
                getById(db, Channel.Companion) {
                    if (it == null) {
                        return@getById
                    }
                    if (!it.users.any{ u -> u.id == user.id }) {
                        resp = HttpStatusCode.Forbidden
                        return@getById
                    }
                    if (it.users.count() != 1L) {
                        resp = HttpStatusCode.Forbidden
                        return@getById
                    }
                    it.users = SizedCollection(listOf())
                    it.delete()
                }
                call.respond(resp)
            }
        }

    }
}