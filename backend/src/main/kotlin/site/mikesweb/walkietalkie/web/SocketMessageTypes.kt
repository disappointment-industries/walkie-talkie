package site.mikesweb.walkietalkie.web

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
sealed class Message {
    abstract val userId: String
    abstract val toUserId: String?
}

@Serializable
@SerialName("offer")
data class OfferMessage(
    val sdp: String,
    override val userId: String,
    override val toUserId: String,
): Message()

@Serializable
@SerialName("answer")
data class AnswerMessage(
    val sdp: String,
    override val userId: String,
    override val toUserId: String,
): Message()

@Serializable
@SerialName("candidate")
data class CandidateMessage(
    val sdp: String,
    val label: Long,
    val id: String,
    override val userId: String,
    override val toUserId: String,
): Message()

@Serializable
@SerialName("connect")
data class ConnectMessage(
    val user: String,
    override val userId: String,
    override val toUserId: String? = null,
): Message()

@Serializable
@SerialName("ping")
data class PingMessage(
    override val userId: String,
    override val toUserId: String? = null,
): Message()

@Serializable
@SerialName("disconnect")
data class Disconnect(
    override val userId: String,
    override val toUserId: String? = null,
): Message()


@Serializable
sealed class Response

@Serializable
@SerialName("ping")
object PingResponse: Response()
