package site.mikesweb.walkietalkie.web

import io.ktor.application.*
import io.ktor.auth.*
import io.ktor.auth.jwt.*
import io.ktor.http.cio.websocket.*
import io.ktor.util.pipeline.*
import io.ktor.websocket.*
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import org.jetbrains.exposed.dao.UUIDEntity
import org.jetbrains.exposed.dao.UUIDEntityClass
import org.jetbrains.exposed.dao.exceptions.EntityNotFoundException
import org.jetbrains.exposed.exceptions.ExposedSQLException
import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.SizedIterable
import org.jetbrains.exposed.sql.transactions.transaction
import site.mikesweb.walkietalkie.data.Channel
import site.mikesweb.walkietalkie.data.User
import java.util.*

val PipelineContext<Unit, ApplicationCall>.uuid: UUID?
get() {
    var id: UUID? = null
    try {
        id = UUID.fromString(call.parameters["id"])
    } catch (_: Exception) {}
    return id
}

fun <T: UUIDEntity, R: Any?> getById(db: Database, ec: UUIDEntityClass<T>, uuid: UUID, action: (T?) -> R): R? {
    var ret: R? = null
    try {
        transaction {
            val resp = ec[uuid]
            ret = action(resp)
        }
    } catch (_: EntityNotFoundException) {
        action(null)
    }
    return ret
}

fun <T: UUIDEntity, R: Any?> PipelineContext<Unit, ApplicationCall>.getById(db: Database, ec: UUIDEntityClass<T>, action: (T?) -> R): R? {
    val uuid = uuid
    if (uuid != null) {
        return getById(db, ec, uuid, action)
    } else {
        return action(null)
    }
}

fun <T: UUIDEntity, R: Any?> getAll(db: Database, ec: UUIDEntityClass<T>, count: Int, start: Long, action: (SizedIterable<T>) -> R) {
    transaction {
        val resp = ec.all().limit(count, start)
        action(resp)
    }
}

fun getUser(uid: String?, db: Database, action: (User?) -> User? = {it}): User? {
    var uuid: UUID? = null
    try {
        uuid = UUID.fromString(uid)
    } catch (_: Exception) {}

    if (uuid != null) {
        return getById(db, User.Companion, uuid, action)
    } else {
        return action(null)
    }
}

fun WebSocketServerSession.getUser(db: Database, action: (User?) -> User? = {it}): User? {
    val uid = call.principal<JWTPrincipal>()?.payload?.claims?.get("id")?.asString()
    return getUser(uid, db, action)
}

fun PipelineContext<Unit, ApplicationCall>.getUser(db: Database, action: (User?) -> User? = {it}): User? {
    val uid = call.principal<JWTPrincipal>()?.payload?.claims?.get("id")?.asString()
    return getUser(uid, db, action)
}

suspend inline fun <reified T> WebSocketSession.replyJson(data: T) {
    outgoing.send(Frame.Text(Json.encodeToString(data)))
}