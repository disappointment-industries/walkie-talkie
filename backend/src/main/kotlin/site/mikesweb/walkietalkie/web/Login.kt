package site.mikesweb.walkietalkie.web

import com.auth0.jwt.JWT
import com.auth0.jwt.algorithms.Algorithm
import io.ktor.application.*
import io.ktor.auth.*
import io.ktor.http.*
import io.ktor.response.*
import io.ktor.routing.*
import org.jetbrains.exposed.sql.SqlExpressionBuilder.eq
import org.jetbrains.exposed.sql.transactions.transaction
import site.mikesweb.walkietalkie.data.User
import site.mikesweb.walkietalkie.data.Users
import site.mikesweb.walkietalkie.data.db
import site.mikesweb.walkietalkie.helpers.checkHashMatches
import site.mikesweb.walkietalkie.helpers.toNewHash
import java.util.*
import java.util.concurrent.TimeUnit

fun Application.setupLogin() {
    routing {
        post<LoginData>("/login") {
            db
            var user: User? = null
            var success = false
            transaction {
                user = User.find { Users.name eq it.username }.firstOrNull()
                if (user == null) {
                    user = User.new {
                        name = it.username
                        pwHash = it.password.toNewHash()
                    }
                    success = true
                } else if (it.password.checkHashMatches(user!!.pwHash)) {
                    success = true
                }
            }
            if (!success) {
                call.respond(HttpStatusCode.Unauthorized, "Password does not match!")
                return@post
            }

            val token = JWT.create()
                .withAudience(jwtAudience)
                .withIssuer(jwtIssuer)
                .withClaim("username", user!!.name)
                .withClaim("id", user!!.id.value.toString())
                .withExpiresAt(Date(System.currentTimeMillis() + TimeUnit.DAYS.toMillis(30)))
                .sign(Algorithm.HMAC256(jwtSecret))

            call.respond(mapOf(
                "userid" to user!!.id.value.toString(),
                "token" to token
            ))
        }
        authenticate {
            get("/loggedIn") {
                val user = getUser(db)
                if (user == null) {
                    call.respond(HttpStatusCode.NotAcceptable)
                } else {
                    call.respond(HttpStatusCode.OK)
                }
            }
        }
    }
}