package site.mikesweb.walkietalkie.web

import io.ktor.http.cio.websocket.*
import io.ktor.websocket.*
import java.time.*
import io.ktor.application.*
import io.ktor.auth.*
import io.ktor.routing.*
import kotlinx.coroutines.async
import kotlinx.coroutines.delay
import java.util.*
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.json.Json
import org.jetbrains.exposed.sql.transactions.transaction
import site.mikesweb.walkietalkie.data.*


private val Connections = mutableSetOf<Connection>()

fun Application.configureSockets() {
    install(WebSockets) {
        pingPeriod = Duration.ofSeconds(15)
        timeout = Duration.ofSeconds(15)
        maxFrameSize = Long.MAX_VALUE
        masking = false
    }

    routing {
        authenticate {
            webSocket("/channels/{id}/socket") {
                val id = call.parameters["id"]
                val currentUser = getUser(db)!! // TODO handle failure
                lateinit var channel: Channel
                getById(db, Channel.Companion, UUID.fromString(id!!)) { ch ->
                    channel = ch!!
                    if (!channel.users.contains(currentUser)) {
                        return@getById // TODO proper message
                    }
                }

                val conn = Connection(
                    id = UUID.randomUUID(),
                    user = currentUser,
                    ch = channel,
                    ws = this,
                )
                Connections += conn

                println("New WebSocket connection")
                async {
                    while (true) {
                        val ping: Response = PingResponse
                        replyJson(ping)
                        delay(1000)
                    }
                }

                for (frame in incoming) {
                    when (frame) {
                        is Frame.Text -> {
                            val text = frame.readText()

                            val message = Json.decodeFromString<Message>(text)

                            when (message) {
                                is PingMessage -> {
                                    val resp: Response = PingResponse
                                    replyJson(resp)
                                }
                                is Disconnect -> {
                                    Connections -= conn
                                    close(CloseReason(CloseReason.Codes.NORMAL, "Client sent disconnect"))
                                }
                                else -> {
                                    Connections.filter { it != conn }.forEach {
                                        it.ws.replyJson(message)
                                    }
                                }
                            }
                        }
                        else -> {}
                    }
                }
                Connections -= conn
                println("WebSocket disconnected")
            }
        }
    }
}

data class Connection(
    val id: UUID,
    val user: User,
    val ch: Channel,
    val ws: WebSocketSession,
)
