package site.mikesweb.walkietalkie.web

import kotlinx.serialization.Serializable
import site.mikesweb.walkietalkie.data.Channel
import site.mikesweb.walkietalkie.data.User

@Serializable
data class RespUser(
    val username: String,
    val id: String,
)

val User.respUser
get() = RespUser(
    this.name,
    this.id.value.toString()
)

@Serializable
data class RespChannel(
    val id: String,
    val name: String,
    val members: List<RespUser>,
)

val Channel.respChannel
get() = RespChannel(
    id = this.id.toString(),
    name = this.name,
    members = this.users.map { user -> user.respUser }
)


