# Walkie-Talkie alkalmazás backend

A MobWeb tárgyhoz készült Androidos Walkie Talkie appomhoz készült backend, melyet
a Kotlin alapú szoftverfejlesztés tárgyhoz készítettem.
A backend Ktor-t használ, mint webszerver framework, illetve Exposed-ot, mint ORM.
A mobil app megtalálható:

- https://play.google.com/store/apps/details?id=site.mikesweb.walkietalkie
- https://gitlab.com/disappointment-industries/walkie-talkie/-/tree/main/android

## WebRTC

Az alkalmazás a hívásokat WebRTC protokollon át bonyolítja le. A WebRTC
signaling-et úgy implementáltam, hogy WebSocket-en át viszem a backend-re az SDP üzeneteket,
melyeket a backend broadcastol a többi fél felé. Ezek után peer-to-peer kapcsolatokat
építenek ki a kliensek egymás közt az SDP üzenetek alapján.

A WebRTC működése dióhéjban:

1. A kliens létrehoz egy PeerConnection-t, amihez hozzáadja a helyi adat streameket (jelen esetben hangot)
2. A kliens készít egy Offer üzenetet, ami egy híváskezdeményezést jelent.
3. A kliens lekéri a STUN szerverektől a publikus IP/port párosát (NAT traversal). (Ezt ICE candidate-nek hívja a STUN és WebRTC)
4. A kliens a kapott IP/port párost hozzáteszi az Offerhez, majd ezt eljuttatja a backendhez.
5. A backend eljuttatja az Offer üzenetet a hívott félhez.
6. A hívott fél is létrehoz egy PeerConnection-t
7. A hívott elmenti a kapott IP/port párost a PeerConnection-be, mint távoli fél.
8. A hívott lekéri a saját IP/port párosát a STUN szerverektől.
9. A hívott létrehoz egy Answer üzenetet, amibe beleteszi az IP/port párosát, majd elküldi WebSocket-en át.
10. A hívó megkapja az Answer üzenetet, a PeerConnection-be elmenti, mint távoli fél.
11. Ezáltal kiépült a peer-to-peer kapcsolat a két fél között.

A WebRTC megértése és leimplementálása egy izgalmas kihívás volt.
Szerencsére a Ktor és Exposed egy kényelmes környezetet adtak ennek megtanulására.

## Futtatás

Az alkalmazásnak szüksége van egy PostgreSQL szerverre, amibe az adatokat tárolja.
Ehhez alapértelmezetten a `postgresql://postgres:postgres@localhost:5432/walkie`
connection string-gel próbál csatlakozni, de ez felülírható a `POSTGRES` env varban.
Emellett opcionálisan egy TURN szerverre is szüksége van, ha minden esetben szeretnénk tudni
átütni NAT-on. TURN szerverhez sajnos publikus IP kell, de az alkalmazás anélkül is tesztelhető.

Mindenesetre, az alkalmazás mindenestül hostolva van a [walkietalkie.mikesweb.site](https://walkietalkie.mikesweb.site)-on.
