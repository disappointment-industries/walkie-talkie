import org.mikeneck.graalvm.GenerateNativeImageConfigTask
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

val ktor_version: String by project
val kotlin_version: String by project
val logback_version: String by project
val exposed_version: String by project

plugins {
    application
    kotlin("jvm") version "1.6.0"
    id("org.jetbrains.kotlin.plugin.serialization") version "1.6.0"
    id("com.github.johnrengelman.shadow") version "7.1.0"
    id("org.mikeneck.graalvm-native-image") version "v1.4.0"
}

group = "site.mikesweb"
version = "0.0.1"
application {
    mainClass.set("site.mikesweb.walkietalkie.ApplicationKt")
}

repositories {
    mavenCentral()
}

tasks.withType<Jar> {
    manifest {
        from("src/main/resources/META-INF/MANIFEST.MF")
    }
}

nativeImage {
    graalVmHome = System.getenv("JAVA_HOME") ?: "/usr/lib/jvm/java-16-graalvm"
    buildType { build ->
        build.executable(main = "site.mikesweb.walkietalkie.ApplicationKt")
    }
    executableName = "walkietalkie"
    outputDirectory = file("$buildDir/libs")
    arguments(
        "--no-fallback",
        "--enable-all-security-services",
        "--report-unsupported-elements-at-runtime",
        "--install-exit-handlers",
        "--allow-incomplete-classpath",
        "--initialize-at-build-time=io.ktor,kotlinx,kotlin,org.slf4j",
        "--initialize-at-build-time=ch.qos.logback",
        "--initialize-at-build-time=javax.xml.parsers.FactoryFinder",
        "--initialize-at-build-time=jdk.xml.internal",
        "--initialize-at-build-time=com.sun.xml.internal",
        "--initialize-at-build-time=com.sun.org.apache.xerces",
        "--initialize-at-build-time=org.postgresql",
        "--initialize-at-build-time=java.sql.DriverManager",
        "--initialize-at-build-time=org.jetbrains.exposed",
        "--initialize-at-build-time=site.mikesweb.walkietalkie",
        "--initialize-at-build-time=kotlinx.serialization.internal.GeneratedSerializer",
        "-H:+ReportUnsupportedElementsAtRuntime",
        "-H:+ReportExceptionStackTraces",
        "-H:ReflectionConfigurationFiles=./reflection.json",
        "-H:ResourceConfigurationFiles=./resources.json",
        "--features=site.mikesweb.walkietalkie.helpers.GraalFeature",
    )
    // jarTask = tasks.withType<Jar>().getByName("shadowJar")
}

kotlin.sourceSets.all {
    languageSettings.optIn("ExperimentalSerializationApi")
}

dependencies {
    implementation("org.jetbrains.exposed:exposed-core:$exposed_version")
    implementation("org.jetbrains.exposed:exposed-dao:$exposed_version")
    implementation("org.jetbrains.exposed:exposed-jdbc:$exposed_version")

    implementation("io.ktor:ktor-server-core:$ktor_version")
    implementation("io.ktor:ktor-auth:$ktor_version")
    implementation("io.ktor:ktor-auth-jwt:$ktor_version")
    implementation("io.ktor:ktor-server-host-common:$ktor_version")
    implementation("io.ktor:ktor-serialization:$ktor_version")
    implementation("io.ktor:ktor-websockets:$ktor_version")
    implementation("io.ktor:ktor-server-cio:$ktor_version")
    implementation("ch.qos.logback:logback-classic:$logback_version")
    testImplementation("io.ktor:ktor-server-tests:$ktor_version")
    testImplementation("org.jetbrains.kotlin:kotlin-test-junit:$kotlin_version")
    implementation("io.ktor:ktor-locations:$ktor_version")
    implementation("org.mindrot:jbcrypt:0.4")
    implementation("org.postgresql:postgresql:42.3.1")
}
val compileKotlin: KotlinCompile by tasks
compileKotlin.kotlinOptions {
    languageVersion = "1.4"
}