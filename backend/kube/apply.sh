#!/bin/bash

set -o allexport
source secret.env
set +o allexport

envsubst < values.yaml | helm install walkietalkie . --values -