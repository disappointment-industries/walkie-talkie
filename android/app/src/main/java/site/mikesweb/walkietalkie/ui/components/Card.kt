package site.mikesweb.walkietalkie.ui.components

import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp

val shape = RoundedCornerShape(12.dp)

/**
 * Unfortunately Material You in Compose is still in it's early stages,
 * and so it doesn't contain a Card composable yet.
 */
@Composable
fun Card(
    modifier: Modifier = Modifier,
    color: Color = MaterialTheme.colorScheme.secondaryContainer,
    elevation: Dp = 0.dp,
    onClick: (() -> Unit) = {},
    content: @Composable () -> Unit = {},
) {
    Surface(
        color = color,
        modifier = modifier,
        onClick = onClick,
        shape = shape,
        tonalElevation = elevation,
    ) {
        content()
    }
}