package site.mikesweb.walkietalkie.api

import java.io.Serializable

data class LoginData(
    val username: String,
    val password: String,
)

data class LoginResponse(
    val userid: String,
    val token: String,
)


data class Channel(
    val id: String,
    val name: String,
    val members: List<User>,
): Serializable

data class User(
    val username: String,
    val id: String,
): Serializable

data class CreateChannel(
    val name: String,
)

fun Channel.hasMember(u: User): Boolean {
    return members.contains(u)
}

data class TurnConfig(
    val username: String,
    val password: String,
    val server: String,
    val expire: Long,
)