package site.mikesweb.walkietalkie.views

import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material3.Icon
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import site.mikesweb.walkietalkie.api.User
import site.mikesweb.walkietalkie.R
import java.text.MessageFormat

@Composable
fun MembersList(members: List<User>) {
    Column(
        modifier = Modifier
            .padding(16.dp)
            .fillMaxWidth(),
        verticalArrangement = Arrangement.spacedBy(16.dp)
    ) {
        Row(
            modifier = Modifier
                .fillMaxWidth(),
            horizontalArrangement = Arrangement.spacedBy(16.dp)
        ) {
            Icon(painter = painterResource(R.drawable.members), contentDescription = "Members")

            val fmt = stringResource(R.string.members)
            Text(MessageFormat.format(fmt, members.size))
        }
        LazyColumn(
            modifier = Modifier
                .padding(16.dp),
            verticalArrangement = Arrangement.spacedBy(16.dp),
            horizontalAlignment = Alignment.Start,
        ) {
            items(
                key = { member -> member.id },
                items = members,
                itemContent = { member ->
                    Row(
                        horizontalArrangement = Arrangement.spacedBy(8.dp, Alignment.Start)
                    ){
                        Icon(painter = painterResource(R.drawable.person), contentDescription = "Person icon")
                        Text(member.username)
                    }
                }
            )
        }
    }
}