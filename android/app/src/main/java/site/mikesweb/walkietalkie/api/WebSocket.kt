package site.mikesweb.walkietalkie.api

import com.tinder.scarlet.*
import com.tinder.scarlet.lifecycle.LifecycleRegistry
import com.tinder.scarlet.messageadapter.gson.GsonMessageAdapter
import com.tinder.scarlet.websocket.okhttp.newWebSocketFactory
import com.tinder.scarlet.ws.Receive
import com.tinder.scarlet.ws.Send
import com.tinder.streamadapter.coroutines.CoroutinesStreamAdapterFactory
import kotlinx.coroutines.channels.ReceiveChannel
import okhttp3.OkHttpClient
import java.io.Closeable


interface WebSocket {
    @Receive
    fun observeWebSocketEvent(): ReceiveChannel<com.tinder.scarlet.WebSocket.Event>
    @Send
    fun sendMessage(msg: Message)
    @Receive
    fun observeMessages(): ReceiveChannel<Message>
}

class WebSocketApi(
    val api: Api,
    val channel: Channel,
    private val lifecycle: LifecycleRegistry = LifecycleRegistry()
): WebSocket by
    Scarlet.Builder()
        .webSocketFactory(
            OkHttpClient.Builder()
                .addInterceptor(TokenInterceptor(api.token!!))
                .build().newWebSocketFactory("${api.server.replace("http", "ws")}/channels/${channel.id}/socket")
        )
        .lifecycle(lifecycle)
        .addMessageAdapterFactory(GsonMessageAdapter.Factory())
        .addStreamAdapterFactory(CoroutinesStreamAdapterFactory())
        .build()
        .create(),
    Closeable {

    init {
        start()
    }

    override fun close() {
        lifecycle.onNext(Lifecycle.State.Stopped.WithReason(ShutdownReason.GRACEFUL))
    }
    fun start() {
        lifecycle.onNext(Lifecycle.State.Started)
    }
}

