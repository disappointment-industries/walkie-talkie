package site.mikesweb.walkietalkie.helpers

import android.content.Context
import java.lang.IllegalArgumentException
import kotlin.properties.PropertyDelegateProvider
import kotlin.properties.ReadWriteProperty
import kotlin.reflect.KProperty

/**
 * A Kotlin Delegate which saves to and reads from SharedPreferences.
 * The name of the SharedPreferences entry is the name of the variable.
 * A Context needs to be passed, either as a parameter, or if the variable
 * is declared inside a Context, then that Context can be used automatically.
 */
fun sharedPref(context: Context? = null): PropertyDelegateProvider<Any?, ReadWriteProperty<Any?, String?>> = SharedPrefFactory(context)

private class SharedPref(private val context: Context, private val key: String): ReadWriteProperty<Any?, String?> {
    private val pref by lazy {
        context.getSharedPreferences("site.mikesweb.walkietalkie", Context.MODE_PRIVATE)!!
    }

    override fun getValue(thisRef: Any?, property: KProperty<*>): String? {
        return pref.getString(key, null)
    }

    override fun setValue(thisRef: Any?, property: KProperty<*>, value: String?) {
        pref.edit().putString(key, value).apply()
    }
}

private class SharedPrefFactory(val context: Context? = null): PropertyDelegateProvider<Any?, ReadWriteProperty<Any?, String?>> {
    override fun provideDelegate(
        thisRef: Any?,
        property: KProperty<*>
    ): ReadWriteProperty<Any?, String?> {
        val ctx: Context = when {
            context != null -> context
            thisRef is Context -> thisRef
            else -> throw IllegalArgumentException("A context is needed")
        }

        return SharedPref(ctx, property.name)
    }
}
