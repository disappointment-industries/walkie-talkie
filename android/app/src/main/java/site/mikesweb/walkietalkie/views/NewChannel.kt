package site.mikesweb.walkietalkie.views

import androidx.compose.material.OutlinedTextField
import androidx.compose.material3.AlertDialog
import androidx.compose.material3.Button
import androidx.compose.material3.Text
import androidx.compose.material.Text as M2Text
import androidx.compose.runtime.*
import androidx.compose.ui.res.stringResource
import kotlinx.coroutines.launch
import site.mikesweb.walkietalkie.R
import site.mikesweb.walkietalkie.api.Api
import site.mikesweb.walkietalkie.api.Channel
import site.mikesweb.walkietalkie.api.CreateChannel
import site.mikesweb.walkietalkie.ui.components.textFieldColors

sealed class Response
class Fail(val exception: Exception): Response()
class Success(val newChannel: Channel): Response()
object Cancel: Response()

@Composable
fun NewChannelDialog(api: Api, after: (Response) -> Unit) {
    var name by remember { mutableStateOf("") }
    val scope = rememberCoroutineScope()

    AlertDialog(
        onDismissRequest = {
            after(Cancel)
        },
        title = {
            Text(stringResource(R.string.newChannel))
        },
        confirmButton = {
            Button(onClick = {
                scope.launch {
                    try {
                        val ch = api.createChannel(CreateChannel(name = name))
                        after(Success(ch))
                    } catch (e: Exception) { after(Fail(e)) }
                }
            }) {
                Text(stringResource(R.string.createChannel))
            }
        },
        text = {
            OutlinedTextField(
                colors = textFieldColors,
                value = name,
                onValueChange = { name = it },
                label = { M2Text(stringResource(R.string.channelName)) },
            )
        }
    )
}