package site.mikesweb.walkietalkie.helpers

import org.webrtc.DataChannel
import org.webrtc.IceCandidate
import org.webrtc.MediaStream
import org.webrtc.PeerConnection

/**
 * The WebRTC library doesn't come with stubs for their interfaces,
 * and since I only use 2 of these functions, I have created this stub.
 */
open class StubPeerConnectionObserver: PeerConnection.Observer {
    override fun onSignalingChange(state: PeerConnection.SignalingState?) { }

    override fun onIceConnectionChange(state: PeerConnection.IceConnectionState?) { }

    override fun onIceConnectionReceivingChange(receiving: Boolean) { }

    override fun onIceGatheringChange(state: PeerConnection.IceGatheringState?) { }

    override fun onIceCandidate(candidate: IceCandidate?) { }

    override fun onIceCandidatesRemoved(candidates: Array<out IceCandidate>?) { }

    override fun onAddStream(stream: MediaStream?) { }

    override fun onRemoveStream(stream: MediaStream?) { }

    override fun onDataChannel(data: DataChannel?) { }

    override fun onRenegotiationNeeded() { }
}