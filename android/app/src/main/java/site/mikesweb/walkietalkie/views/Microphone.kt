package site.mikesweb.walkietalkie.views

import androidx.compose.animation.animateColor
import androidx.compose.animation.core.updateTransition
import androidx.compose.foundation.gestures.detectTapGestures
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.input.pointer.pointerInput
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import com.google.accompanist.permissions.ExperimentalPermissionsApi
import com.google.accompanist.permissions.PermissionRequired
import com.google.accompanist.permissions.rememberPermissionState
import site.mikesweb.walkietalkie.LocalSoundHelper
import site.mikesweb.walkietalkie.R
import site.mikesweb.walkietalkie.helpers.*
import site.mikesweb.walkietalkie.helpers.State

@Composable
@OptIn(ExperimentalPermissionsApi::class)
fun Microphone() {
    var soundHelperState by remember { mutableStateOf<State>(WebSocketConnecting) }
    val microphonePermissionState = rememberPermissionState(android.Manifest.permission.RECORD_AUDIO)
    PermissionRequired(
        permissionState = microphonePermissionState,
        permissionNotGrantedContent = {
            Content(on = false){
                Button(
                    onClick = {
                        microphonePermissionState.launchPermissionRequest()
                    }
                ) {
                    Text(stringResource(R.string.grant))
                }
            }
        },
        permissionNotAvailableContent = {
            Content(on = false)
        }
    ) {
        val soundHelper = LocalSoundHelper.current.soundHelper
        LaunchedEffect(true) {
            soundHelper?.onStateChange = {
                soundHelperState = it
            }
        }
        if (soundHelper?.started != true) {
            soundHelper?.start()
        }
        val state = soundHelperState
        Content(
            on = true,
            customText = when (state) {
                is WebSocketConnecting -> stringResource(R.string.webSocketConnecting)
                is WebRTCConnecting -> stringResource(R.string.webRTCConnecting)
                is WebRTCConnected -> stringResource(R.string.mic_on)
                is Error -> state.error.toString()
                is Closed -> stringResource(R.string.commClosed)
                else -> null
            }
        ) {
            SpeakerButton {
                soundHelper?.isSpeakerPhone = it
            }
            PTTButton() {
                soundHelper?.talk = it
            }
        }
    }
}

@Composable
private fun Content(on: Boolean, customText: String? = null, extra: @Composable () -> Unit = {}) {
    Column(
        modifier = Modifier
            .fillMaxWidth()
            .padding(16.dp),
        verticalArrangement = Arrangement.spacedBy(8.dp),
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Row(
            modifier = Modifier.fillMaxWidth(),
            horizontalArrangement = Arrangement.Center
        ) {
            Icon(
                painter = if (on) painterResource(R.drawable.mic_on) else painterResource(R.drawable.mic_off),
                contentDescription = "Microphone icon"
            )
            val txt = customText
                ?: if (on)
                    stringResource(R.string.connecting)
                else
                    stringResource(R.string.mic_off)

            Text(txt)
        }
        extra()
    }
}

@Composable
private fun SpeakerButton(onChange: (Boolean) -> Unit){
    var speaker by remember {
        mutableStateOf(false )
    }
    IconToggleButton(checked = speaker, onCheckedChange = {
        speaker = it
        onChange(it)
    }) {
        val transition = updateTransition(speaker, label = "Checked indicator")
        val tint by transition.animateColor(label = "Tint") { checked ->
            if (checked) MaterialTheme.colorScheme.tertiaryContainer else MaterialTheme.colorScheme.onSecondaryContainer
        }
        val innerTint by transition.animateColor(label = "Tint") { checked ->
            if (checked) MaterialTheme.colorScheme.onTertiaryContainer else MaterialTheme.colorScheme.secondaryContainer
        }
        Surface(
            modifier = Modifier.clip(CircleShape),
            color = tint,
        ) {
            Icon(
                modifier = Modifier.padding(8.dp),
                painter = painterResource(R.drawable.speakerphone),
                contentDescription = "Speakerphone",
                tint = innerTint,
            )
        }
    }
}

val pttShape = RoundedCornerShape(16.dp)

@Composable
fun PTTButton(onChange: (Boolean) -> Unit) {

    var btnPressed by remember { mutableStateOf(false) }

    val transition = updateTransition(btnPressed, label = "Checked indicator")
    val tint by transition.animateColor(label = "Tint") { pressed ->
        if (pressed) MaterialTheme.colorScheme.tertiaryContainer else MaterialTheme.colorScheme.primaryContainer
    }
    val innerTint by transition.animateColor(label = "Tint") { pressed ->
        if (pressed) MaterialTheme.colorScheme.onTertiaryContainer else MaterialTheme.colorScheme.onPrimaryContainer
    }

    val colors = ButtonDefaults.buttonColors(
        containerColor = tint,
        contentColor = innerTint,
    )

    Surface(
        modifier = Modifier
            .pointerInput(Unit){
                detectTapGestures(
                    onPress = {
                        btnPressed = true
                        onChange(true)
                        this.awaitRelease()
                        btnPressed = false
                        onChange(false)
                    }
                )
           },
        shape = pttShape,
        color = colors.containerColor(enabled = true).value,
        contentColor = colors.contentColor(enabled = true).value.copy(alpha = 1f),
    ) {
        Text(
            text = "Push To Talk",
            modifier = Modifier.padding(16.dp),
        )
    }
}