package site.mikesweb.walkietalkie.views

import androidx.compose.foundation.layout.*
import androidx.compose.material3.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import kotlinx.coroutines.launch
import site.mikesweb.walkietalkie.LocalSoundHelper
import site.mikesweb.walkietalkie.R
import site.mikesweb.walkietalkie.api.Api
import site.mikesweb.walkietalkie.api.Channel
import site.mikesweb.walkietalkie.api.User
import site.mikesweb.walkietalkie.api.hasMember
import site.mikesweb.walkietalkie.ui.components.Card

@Composable
fun ChannelView(
    channel: Channel,
    user: User,
    api: Api,
    onWarning: (String) -> Unit = {},
    onError: (Exception) -> Unit,
    onDelete: () -> Unit,
    onUpdate: () -> Unit
) {
    suspend fun handleError(f: suspend () -> Unit){
        try {
            f()
        } catch (e: Exception) {
            onError(e)
        }
    }

    val scope = rememberCoroutineScope()

    Card(
        modifier = Modifier
            .fillMaxWidth()
            .padding(16.dp),
    ) {
        Column {
            MembersList(channel.members)
            Row(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(16.dp),
                horizontalArrangement = Arrangement.SpaceEvenly,
            ) {
                val modifier = Modifier

                if (channel.hasMember(user)) {
                    Button(
                        onClick = {
                            scope.launch {
                                handleError {
                                    api.leaveChannel(channel.id)
                                    onUpdate()
                                }
                            }
                        },
                        modifier = modifier,
                    ) {
                        Text(stringResource(R.string.leave))
                    }
                } else {
                    Button(
                        onClick = {
                            scope.launch {
                                handleError {
                                    api.joinChannel(channel.id)
                                    onUpdate()
                                }
                            }
                        },
                        modifier = modifier,
                    ) {
                        Text(stringResource(R.string.join))
                    }
                }

                val deleteWarning = stringResource(R.string.cantDelete)
                val deleteEnabled = channel.hasMember(user) && channel.members.size == 1
                val colors = ButtonDefaults.buttonColors(containerColor = MaterialTheme.colorScheme.error)
                Button(
                    onClick = {
                        if (deleteEnabled) {
                            scope.launch {
                                handleError {
                                    api.deleteChannel(channel.id)
                                    onDelete()
                                }
                            }
                        } else {
                            onWarning(deleteWarning)
                        }
                    },
                    modifier = modifier,
                    colors = if (deleteEnabled)
                        colors
                    else
                        ButtonDefaults.buttonColors(
                            containerColor = colors.containerColor(false).value,
                            contentColor = colors.contentColor(false).value,
                        ),
                ) {
                    Text(stringResource(R.string.deleteChannel))
                }
            }
            if (channel.hasMember(user)) {
                Microphone()
            } else {
                LocalSoundHelper.current.soundHelper?.close()
            }
        }
    }
}
