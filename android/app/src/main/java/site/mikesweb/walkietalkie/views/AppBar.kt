package site.mikesweb.walkietalkie.views

import androidx.compose.material3.SmallTopAppBar
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.res.stringResource
import site.mikesweb.walkietalkie.R

@Composable
fun AppBar(
    text: String = stringResource(R.string.app_name)
) {
    SmallTopAppBar(
        title = {
            Text(text)
        }
    )
}