package site.mikesweb.walkietalkie.views

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import site.mikesweb.walkietalkie.api.Channel
import site.mikesweb.walkietalkie.ui.theme.WalkieTalkieTheme
import com.google.accompanist.swiperefresh.*

val fakeChannels = listOf((1..6)).flatten().map {
    Channel(
        id = "$it",
        name = "placeholder".repeat(3),
        members = listOf(),
    )
}

/**
 * This is the Column containing the Channels. It uses LazyColumn,
 * which is the Jetpack Compose equivalent of RecyclerView.
 */
@Composable
fun ChannelColumn(channels: List<Channel>, loaded: Boolean, reload: (swipe: SwipeRefreshState) -> Unit) {
    val isRefreshing by remember { mutableStateOf(SwipeRefreshState(loaded)) }

    SwipeRefresh(
        state = isRefreshing,
        onRefresh = { reload(isRefreshing) }
    ) {
        LazyColumn(
            modifier = Modifier
                .fillMaxSize()
                .padding(16.dp),
            verticalArrangement = Arrangement.spacedBy(16.dp),
            horizontalAlignment = Alignment.CenterHorizontally,
        ) {
            items(
                key = { channel -> channel.id },
                items = channels,
                itemContent = { channel ->
                    ChannelCard(channel)
                }
            )
        }

    }

    if (!loaded) {
        reload(isRefreshing)
    } else {
        // workaround, sometimes isRefreshing resets after recomposing
        isRefreshing.isRefreshing = false
    }
}

@Preview(showBackground = true)
@Composable
fun ChannelPreview() {
    WalkieTalkieTheme {
        ChannelColumn(channels = fakeChannels, loaded = true, reload = {})
    }
}
