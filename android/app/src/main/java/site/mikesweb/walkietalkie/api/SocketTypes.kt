package site.mikesweb.walkietalkie.api


data class Message(
    val type: String,
    val sdp: String? = null,
    val userId: String,
    val toUserId: String? = null,

    // specific to candidate
    val label: Int? = null,
    val id: String? = null,

    // specific to connect
    val user: String? = null,
)


