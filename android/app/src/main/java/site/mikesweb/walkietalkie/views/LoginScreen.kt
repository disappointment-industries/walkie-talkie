package site.mikesweb.walkietalkie.views

import android.view.KeyEvent.KEYCODE_ENTER
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.OutlinedTextField
import androidx.compose.material.SnackbarHost
import androidx.compose.material.SnackbarHostState
import androidx.compose.material3.*
import androidx.compose.material.Text as M2Text
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.focus.FocusRequester
import androidx.compose.ui.focus.focusRequester
import androidx.compose.ui.input.key.onKeyEvent
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.input.PasswordVisualTransformation
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import retrofit2.HttpException
import site.mikesweb.walkietalkie.BuildConfig
import site.mikesweb.walkietalkie.R
import site.mikesweb.walkietalkie.api.Api
import site.mikesweb.walkietalkie.api.LoginData
import site.mikesweb.walkietalkie.ui.components.textFieldColors
import site.mikesweb.walkietalkie.ui.theme.WalkieTalkieTheme

@Composable
@OptIn(ExperimentalMaterial3Api::class, ExperimentalComposeUiApi::class)
fun LoginScreen(onSuccess: (String, String) -> Unit) {
    var srv by remember { mutableStateOf("https://walkietalkie.mikesweb.site") }
    // if it's in debug mode, I fill up the text field so as to not have to type it in
    var username by remember { mutableStateOf(
        if (BuildConfig.DEBUG) "mike" else ""
    ) }
    var password by remember { mutableStateOf(
        if (BuildConfig.DEBUG) "almafa" else ""
    ) }

    val coroutineScope = rememberCoroutineScope()

    val snackbarHostState by remember { mutableStateOf(SnackbarHostState()) }
    val (usernameFocus) = FocusRequester.createRefs()
    val (passwordFocus) = FocusRequester.createRefs()
    
    val badPass = stringResource(R.string.badPass)
    val errorHappened = stringResource(R.string.errorHappened)

    fun login(){
        coroutineScope.launch(Dispatchers.IO) {
            try {
                val token = Api(srv).login(
                    LoginData(
                        username = username,
                        password = password
                    )
                )?.token
                if (token != null)
                    onSuccess(srv, token)
                else
                    throw Exception("Didn't receive token from server")
            } catch (e: HttpException) {
                if (e.code() == 401) {
                    snackbarHostState.showSnackbar(
                        message = badPass,
                    )
                } else {
                    snackbarHostState.showSnackbar(
                        message = e.message ?: errorHappened
                    )
                }
            } catch (e: Exception) {
                snackbarHostState.showSnackbar(
                    message = e.message ?: errorHappened
                )
                e.printStackTrace()
            }
        }
    }

    Scaffold(
        modifier = Modifier.fillMaxSize(),
    ) {
        Surface(color = MaterialTheme.colorScheme.background) {
            Box(
                modifier = Modifier.fillMaxSize()
            ) {
                Column(
                    modifier = Modifier
                        .fillMaxSize()
                        .padding(16.dp),
                    verticalArrangement = Arrangement.spacedBy(16.dp, Alignment.CenterVertically),
                    horizontalAlignment = Alignment.CenterHorizontally,
                ) {
                    OutlinedTextField(
                        keyboardOptions = KeyboardOptions(
                            keyboardType = KeyboardType.Uri,
                            imeAction = ImeAction.Next,
                        ),
                        keyboardActions = KeyboardActions(
                            onNext = { usernameFocus.requestFocus() }
                        ),
                        colors = textFieldColors,
                        value = srv,
                        onValueChange = { srv = it },
                        label = { M2Text(stringResource(R.string.serverName)) },
                        modifier = Modifier.onKeyEvent {
                            if (it.nativeKeyEvent.keyCode == KEYCODE_ENTER) {
                                usernameFocus.requestFocus()
                                true
                            } else {
                                false
                            }
                        }
                    )

                    OutlinedTextField(
                        keyboardOptions = KeyboardOptions(
                            keyboardType = KeyboardType.Ascii,
                            imeAction = ImeAction.Next,
                        ),
                        keyboardActions = KeyboardActions(
                            onNext = { passwordFocus.requestFocus() }
                        ),
                        colors = textFieldColors,
                        value = username,
                        onValueChange = { username = it },
                        label = { M2Text(stringResource(R.string.username)) },
                        modifier = Modifier
                            .focusRequester(usernameFocus)
                            .onKeyEvent {
                                if (it.nativeKeyEvent.keyCode == KEYCODE_ENTER) {
                                    passwordFocus.requestFocus()
                                    true
                                } else {
                                    false
                                }
                            }
                    )

                    OutlinedTextField(
                        keyboardOptions = KeyboardOptions(
                            keyboardType = KeyboardType.Password,
                            imeAction = ImeAction.Done,
                        ),
                        keyboardActions = KeyboardActions(
                            onDone = { login() }
                        ),
                        colors = textFieldColors,
                        value = password,
                        visualTransformation = PasswordVisualTransformation(),
                        onValueChange = { password = it },
                        label = { M2Text(stringResource(R.string.password)) },
                        modifier = Modifier
                            .focusRequester(passwordFocus)
                            .onKeyEvent {
                                if (it.nativeKeyEvent.keyCode == KEYCODE_ENTER) {
                                    login()
                                    true
                                } else {
                                    false
                                }
                            }
                    )

                    Button(
                        onClick = { login() }
                    ) {
                        Text(text = stringResource(R.string.login))
                    }
                }
                Column(
                    modifier = Modifier.fillMaxSize().padding(bottom = 16.dp),
                    verticalArrangement = Arrangement.Bottom
                ) {
                    SnackbarHost(hostState = snackbarHostState)
                }
            }
        }
    }
}

@Preview(showBackground = true)
@Composable
fun LoginPreview() {
    WalkieTalkieTheme {
        ChannelColumn(channels = fakeChannels, loaded = true, reload = {})
    }
}