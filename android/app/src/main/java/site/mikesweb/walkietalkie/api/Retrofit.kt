package site.mikesweb.walkietalkie.api

import androidx.compose.runtime.Stable
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.*
import site.mikesweb.walkietalkie.helpers.Token

@Stable
interface ApiInterface {
    @POST("/login")
    suspend fun login(@Body data: LoginData): LoginResponse?

    @GET("/loggedIn")
    suspend fun loggedIn()

    @GET("/channels")
    suspend fun channels(): List<Channel>

    @POST("/channels")
    suspend fun createChannel(@Body channel: CreateChannel): Channel

    @GET("/channels/{id}")
    suspend fun channel(@Path("id") id: String): Channel

    @POST("/channels/{id}/join")
    suspend fun joinChannel(@Path("id") id: String)

    @POST("/channels/{id}/leave")
    suspend fun leaveChannel(@Path("id") id: String)

    @DELETE("/channels/{id}")
    suspend fun deleteChannel(@Path("id") id: String)

    @GET("/turn")
    suspend fun getTurnConfig(): TurnConfig
}

@Stable
class Api(val server: String, val token: Token? = null): ApiInterface by
    Retrofit.Builder()
        .baseUrl(server)
        .client(
            if (token == null)
                OkHttpClient.Builder().build()
            else
                OkHttpClient.Builder()
                    .addInterceptor(TokenInterceptor(token))
                    .build()
        )
        .addConverterFactory(GsonConverterFactory.create())
        .build().create(ApiInterface::class.java)

class TokenInterceptor(val token: Token): Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        val newRequest = chain.request().newBuilder()
            .addHeader("Authorization", "Bearer ${token.token}")
            .build()
        return chain.proceed(newRequest)
    }

}
