package site.mikesweb.walkietalkie.views

import android.content.Intent
import androidx.compose.foundation.layout.*
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.unit.dp
import site.mikesweb.walkietalkie.ChannelActivity
import site.mikesweb.walkietalkie.api.Channel
import site.mikesweb.walkietalkie.ui.components.Card
import site.mikesweb.walkietalkie.ui.components.Circle

@Composable
fun ChannelCard(channel: Channel) {
    val context = LocalContext.current

    Card (
        onClick = {
            val intent = Intent(context, ChannelActivity::class.java)
            intent.putExtra("channel", channel)
            context.startActivity(intent)
        },
    ) {
        Row(
            modifier = Modifier
                .fillMaxSize()
                .padding(16.dp),
            verticalAlignment = Alignment.CenterVertically,
            horizontalArrangement = Arrangement.spacedBy(16.dp)
        ) {
            Circle {
                Text(
                    text = "${channel.members.size}",
                )
            }

            Text(
                text = channel.name,
            )
        }
    }
}