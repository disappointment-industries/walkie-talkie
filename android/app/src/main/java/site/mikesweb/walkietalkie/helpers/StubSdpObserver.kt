package site.mikesweb.walkietalkie.helpers

import org.webrtc.SdpObserver
import org.webrtc.SessionDescription

/**
 * The WebRTC library doesn't come with stubs for their interfaces,
 * and since I only use 1 of these functions, I have created this stub.
 */
open class StubSdpObserver: SdpObserver {
    override fun onCreateSuccess(desc: SessionDescription?) { }

    override fun onSetSuccess() { }

    override fun onCreateFailure(fail: String?) { }

    override fun onSetFailure(fail: String?) { }
}