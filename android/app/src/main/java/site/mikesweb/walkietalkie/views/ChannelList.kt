package site.mikesweb.walkietalkie.views

import android.annotation.SuppressLint
import androidx.compose.animation.Crossfade
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.material.SnackbarHost
import androidx.compose.material.SnackbarHostState
import androidx.compose.material3.*
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import site.mikesweb.walkietalkie.R
import site.mikesweb.walkietalkie.api.Api
import site.mikesweb.walkietalkie.api.Channel
import site.mikesweb.walkietalkie.api.hasMember
import site.mikesweb.walkietalkie.helpers.toUser

/**
 * This is the screen which contains and loads the Channels.
 * It is an object, because we want to be able to refresh it from different parts of our app.
 */
object ChannelList {
    /**
     * I used the Kotlin invoke operator "()", so that this
     * object could be used as a function. Unfortunately the Compose linter
     * thinks that the usage would look like ".invoke()", so that message is suppressed.
     */
    @Composable
    @SuppressLint("ComposableNaming")
    @OptIn(ExperimentalMaterial3Api::class)
    operator fun invoke(api: Api) {
        var dialogOpen by remember { mutableStateOf(false) }

        val snackbarHostState by remember { mutableStateOf(SnackbarHostState()) }

        var channels by remember { mutableStateOf(mutableListOf<Channel>()) }
        var loaded by remember { mutableStateOf(false) }
        val scope = rememberCoroutineScope()
        var error by remember { mutableStateOf<Exception?>(null) }

        suspend fun refresh() {
            val user = api.token!!.data.toUser()
            try {
                channels = api.channels().sortedWith( compareBy({ !it.hasMember(user) }, { it.name.lowercase() }) )
                    .toMutableList()
                loaded = true
                error = null
            } catch (e: Exception) {
                error = e
            }
        }
        refresherFunc = { refresh() }

        Surface(color = MaterialTheme.colorScheme.background) {
            Scaffold(
                modifier = Modifier
                    .background(MaterialTheme.colorScheme.background)
                    .fillMaxSize(),
                topBar = {
                    AppBar()
                },
                floatingActionButton = {
                    Crossfade(targetState = error) { currentError ->
                        if (currentError == null) {
                            ExtendedFloatingActionButton(
                                containerColor = MaterialTheme.colorScheme.secondaryContainer,
                                onClick = {
                                    dialogOpen = true
                                },
                                text = {
                                    Text(
                                        text = stringResource(R.string.newChannel),
                                    )
                                },
                                icon = {
                                    Icon(painter = painterResource(R.drawable.plus), contentDescription = "Plus icon")
                                }
                            )
                        } else {
                            /* This is needed, because if we don't draw anything,
                             * something wild happens in Compose and after pressing the "try again"
                             * button, the FAB wouldn't come back.
                             * Also, empty text doesn't work either.
                             */
                            Text(
                                text = "placeholder",
                                color = Color.Transparent,
                            )
                        }
                    }
                },
            ) {
                Box(
                    modifier = Modifier.fillMaxSize(),
                ) {
                    Crossfade(targetState = error) { currentError ->
                        if (currentError != null) {
                            ErrorScreen(reason = currentError, location = "ChannelList") {
                                scope.launch(Dispatchers.IO) {
                                    refresh()
                                }
                            }
                        } else {
                            ChannelColumn(
                                channels = channels,
                                loaded = loaded,
                                reload = { isRefreshing ->
                                    isRefreshing.isRefreshing = true
                                    scope.launch(Dispatchers.IO) {
                                        refresh()
                                        delay(100)
                                        isRefreshing.isRefreshing = false
                                    }
                                }
                            )
                        }
                    }
                    Column(
                        modifier = Modifier.fillMaxSize().padding(bottom = 64.dp),
                        verticalArrangement = Arrangement.Bottom
                    ) {
                        SnackbarHost(hostState = snackbarHostState)
                    }
                }
            }
        }

        if (dialogOpen) {
            val failMsg = stringResource(R.string.channelAddFail)
            NewChannelDialog(api) { response ->
                dialogOpen = false
                when (response) {
                    is Success -> channels.add(0, response.newChannel)
                    is Fail -> scope.launch {
                        snackbarHostState.showSnackbar(failMsg)
                    }
                    is Cancel -> {}
                }
            }
        }
    }

    /**
     * This is so that we can refresh this screen if we update some stuff,
     * even from different parts of our app,
     * say we delete a channel from ChannelActivity.
     */
    private var refresherFunc: suspend () -> Unit = {}
    suspend fun refresh() {
        refresherFunc()
    }
}