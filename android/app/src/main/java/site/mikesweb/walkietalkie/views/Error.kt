package site.mikesweb.walkietalkie.views

import androidx.compose.foundation.layout.*
import androidx.compose.material3.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import site.mikesweb.walkietalkie.BuildConfig
import site.mikesweb.walkietalkie.R

@Composable
fun ErrorScreen(
    reason: Exception? = null,
    location: String? = null,
    icon: Int = R.drawable.offline,
    actionText: String = stringResource(R.string.retry),
    action: (() -> Unit)? = null,
) {
    Surface(
        color = MaterialTheme.colorScheme.background,
    ) {
        Column(
            modifier = Modifier
                .padding(16.dp)
                .fillMaxSize(),
            verticalArrangement = Arrangement.spacedBy(16.dp, Alignment.CenterVertically),
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            Icon(
                painter = painterResource(icon),
                contentDescription = "Error icon",
                modifier = Modifier.size(128.dp),
            )
            Text(
                text = stringResource(R.string.error),
                style = MaterialTheme.typography.headlineMedium,
            )
            if (reason != null) {
                Text(reason.toString())
            }
            if (action != null) {
                TextButton(onClick = { action() }) {
                    Text(actionText)
                }
            }
            if(BuildConfig.DEBUG && reason != null) {
                Text(reason.stackTraceToString())
                Text(location ?: "")
            }
        }
    }
}