package site.mikesweb.walkietalkie.helpers

import android.util.*
import androidx.compose.runtime.Stable
import kotlinx.serialization.Serializable
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.json.Json
import site.mikesweb.walkietalkie.api.User

@Serializable
@Stable
data class TokenData(
    val username: String,
    val id: String,
    val exp: Int,
)

private val json = Json {
    ignoreUnknownKeys = true
}

/**
 * This class decodes a JWT token.
 * the @Stable annotation means that Jetpack Compose doesn't
 * need to redraw the application if we use any members of this class.
 */
@Stable
class Token(val token: String): Comparable<Token> {
    val data: TokenData
    init {
        val jsonStr = String(Base64.decode(token.split(".")[1], Base64.DEFAULT))
        data = json.decodeFromString(jsonStr)
    }

    override operator fun compareTo(other: Token): Int {
        return token.compareTo(other.token)
    }

    override fun equals(other: Any?): Boolean {
        if (other !is Token) return false
        return token == other.token
    }

    override fun hashCode(): Int {
        return token.hashCode()
    }
}

fun String.toToken() = Token(this)

fun TokenData.toUser() = User(
    id = id,
    username = username,
)