package site.mikesweb.walkietalkie

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.animation.Crossfade
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.material.SnackbarHost
import androidx.compose.material.SnackbarHostState
import androidx.compose.material3.*
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import site.mikesweb.walkietalkie.api.Api
import site.mikesweb.walkietalkie.api.Channel
import site.mikesweb.walkietalkie.api.WebSocketApi
import site.mikesweb.walkietalkie.helpers.SoundHelper
import site.mikesweb.walkietalkie.helpers.sharedPref
import site.mikesweb.walkietalkie.helpers.toToken
import site.mikesweb.walkietalkie.helpers.toUser
import site.mikesweb.walkietalkie.ui.theme.WalkieTalkieTheme
import site.mikesweb.walkietalkie.views.*


val LocalSoundHelper = compositionLocalOf<SoundHelperContainer> { error("No SoundHelper found!") }

class SoundHelperContainer {
    var soundHelper: SoundHelper? = null
}

class ChannelActivity : ComponentActivity() {
    sealed class State
    private object Loaded: State()
    private class Error(val e: Exception): State()

    private val server by sharedPref()
    private val token by sharedPref()

    private val soundHelperContainer = SoundHelperContainer()

    var soundHelper: SoundHelper?
    get() = soundHelperContainer.soundHelper
    set(value) { soundHelperContainer.soundHelper = value }

    override fun onDestroy() {
        super.onDestroy()

        soundHelper?.close()
        soundHelper = null
    }

    @OptIn(ExperimentalMaterial3Api::class)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val token = token!!.toToken()
        val api = Api(server!!, token)

        val ch = intent.extras?.getSerializable("channel") as? Channel ?: throw IllegalArgumentException("A Channel must be passed as \"channel\"")

        setContent {
            var channel by remember { mutableStateOf(ch) }
            var state by remember { mutableStateOf<State>(Loaded) }
            val scope = rememberCoroutineScope()

            fun refresh() {
                scope.launch(Dispatchers.IO) {
                    try {
                        channel = api.channel(channel.id)
                        state = Loaded
                    } catch (e: Exception) {
                        state = Error(e)
                    }
                }
            }

            if (soundHelper == null) {
                soundHelper = SoundHelper(
                    context = this,
                    socket = WebSocketApi(api, ch),
                    scope = scope,
                )
            }

            CompositionLocalProvider(LocalSoundHelper provides soundHelperContainer) {
                WalkieTalkieTheme {
                    Surface(color = MaterialTheme.colorScheme.background) {
                        val snackbarHostState by remember { mutableStateOf(SnackbarHostState()) }
                        Scaffold(
                            modifier = Modifier
                                .background(MaterialTheme.colorScheme.background)
                                .fillMaxSize(),
                            topBar = {
                                AppBar(channel.name)
                            },
                            content = {
                                val activity = this
                                Box(
                                    modifier = Modifier.fillMaxSize(),
                                ) {
                                    Crossfade(state) { currentState ->
                                        when (currentState) {
                                            is Loaded -> Crossfade(channel) { channel ->
                                                ChannelView(
                                                    channel = channel,
                                                    user = token.data.toUser(),
                                                    onWarning = {
                                                        scope.launch {
                                                            snackbarHostState.showSnackbar(it)
                                                        }
                                                    },
                                                    onError = { state = Error(it) },
                                                    api = api,
                                                    onDelete = {
                                                        scope.launch {
                                                            ChannelList.refresh()
                                                            activity.finish()
                                                        }
                                                    },
                                                    onUpdate = {
                                                        refresh()
                                                        scope.launch {
                                                            ChannelList.refresh()
                                                        }
                                                    }
                                                )
                                            }
                                            is Error -> ErrorScreen(
                                                reason = currentState.e,
                                                location = "ChannelActivity"
                                            ) { refresh() }
                                        }
                                    }
                                    Column(
                                        modifier = Modifier.fillMaxSize().padding(bottom = 64.dp),
                                        verticalArrangement = Arrangement.Bottom
                                    ) {
                                        SnackbarHost(hostState = snackbarHostState)
                                    }
                                }
                            },
                            floatingActionButton = {
                                ExtendedFloatingActionButton(
                                    containerColor = MaterialTheme.colorScheme.secondaryContainer,
                                    text = {
                                        Text(
                                            text = stringResource(R.string.refresh),
                                        )
                                    },
                                    onClick = { refresh() },
                                    icon = {
                                        Icon(painter = painterResource(R.drawable.refresh), contentDescription = "Refresh icon")
                                    }
                                )
                            },
                        )
                    }
                }
            }
        }
    }
}
