package site.mikesweb.walkietalkie

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.animation.Crossfade
import androidx.compose.material3.*
import androidx.compose.runtime.*
import kotlinx.coroutines.NonCancellable
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import retrofit2.HttpException
import site.mikesweb.walkietalkie.api.Api
import site.mikesweb.walkietalkie.helpers.sharedPref
import site.mikesweb.walkietalkie.helpers.toToken
import site.mikesweb.walkietalkie.ui.theme.WalkieTalkieTheme
import site.mikesweb.walkietalkie.views.*

class MainActivity : ComponentActivity() {

    sealed class State
    private class Error(val e: Exception): State()
    private object LoggedIn: State()
    private object LoggedOut: State()
    private object Undefined: State()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContent {
            var server by sharedPref(this)
            var token by sharedPref(this)

            var state by remember { mutableStateOf(
                if (server == null || token == null) LoggedOut else Undefined
            ) }

            WalkieTalkieTheme {
                Crossfade(targetState = state) { currentState ->
                    when (currentState) {
                        is Error -> {
                            ErrorScreen(reason = currentState.e, location = "MainActivity") {
                                state = Undefined
                            }
                        }
                        is Undefined -> {
                            Spinner()
                            LaunchedEffect(false) {
                                withContext(NonCancellable) {
                                    try {
                                        Api(server!!, token!!.toToken()).loggedIn()
                                        state = LoggedIn
                                    } catch (_: HttpException) {
                                        state = LoggedOut
                                    } catch (e: Exception) {
                                        state = Error(e)
                                    }
                                }
                            }
                        }
                        is LoggedIn -> {
                            ChannelList(Api(server!!, token!!.toToken()))
                        }
                        is LoggedOut -> {
                            LoginScreen() { s, t ->
                                server = s
                                token = t
                                state = LoggedIn
                            }
                        }
                    }
                }
            }
        }
    }
}