package site.mikesweb.walkietalkie.helpers

import android.content.Context
import android.media.AudioManager
import kotlinx.coroutines.*
import org.webrtc.*
import site.mikesweb.walkietalkie.BuildConfig
import site.mikesweb.walkietalkie.api.*
import java.io.Closeable

/**
 * A list of public STUN servers, in case my STUN/TURN server isn't available.
 */
private val stunList = listOf(
    "stun:stun.services.mozilla.com:3478",
    "stun:stun.l.google.com:19302",
    "stun:stun1.l.google.com:19302",
    "stun:stun2.l.google.com:19302",
    "stun:stun3.l.google.com:19302",
    "stun:stun4.l.google.com:19302",
    "stun:stun.vodafone.ro:3478",
    "stun:stun.samsungsmartcam.com:3478",
)

/**
 * The state of the connection.
 */
sealed class State
object WebSocketConnecting: State()
object WebRTCConnecting: State()
object WebRTCConnected: State()
object Closed: State()
class Error(val error: Exception): State()

/**
 * The WebRTC implementation itself. This is quite a long class, but I don't think it could reasonably be split up better.
 */
class SoundHelper(val context: Context, val socket: WebSocketApi, val scope: CoroutineScope, var onStateChange: (State) -> Unit = {}): Closeable {
    private val audioManager = context.getSystemService(Context.AUDIO_SERVICE) as AudioManager
    private val peerConnections = mutableMapOf<String, PeerConnection>()
    private val mediaStreams = mutableSetOf<MediaStream>()
    private val localCandidates = mutableListOf<Message>()
    private val audioTrack: AudioTrack
    private val factory: PeerConnectionFactory

    var turn: TurnConfig? = null

    /**
     * This variable controls if the sound is coming out of the earpiece speaker (false)
     * or the loudspeaker (true)
     */
    var isSpeakerPhone: Boolean
        get() = audioManager.mode == AudioManager.MODE_NORMAL
        set(value) {
            if (value) {
                audioManager.mode = AudioManager.MODE_NORMAL
                audioManager.stopBluetoothSco()
                audioManager.isBluetoothScoOn = false
                audioManager.isSpeakerphoneOn = true
            } else {
                audioManager.mode = AudioManager.MODE_IN_CALL
                audioManager.isBluetoothScoOn = true
                audioManager.isSpeakerphoneOn = false
            }
        }

    init {
        var builder = PeerConnectionFactory.InitializationOptions.builder(context)

        if(BuildConfig.DEBUG) {
            builder = builder.setInjectableLogger({ log, sev, _ ->
                println("$sev: $log")
            }, Logging.Severity.LS_VERBOSE)
        }

        PeerConnectionFactory.initialize(builder.createInitializationOptions())
        factory = PeerConnectionFactory.builder().createPeerConnectionFactory()
        val audioSource = factory.createAudioSource(MediaConstraints())
        audioTrack = factory.createAudioTrack("101", audioSource)
        // start with the microphone muted
        audioTrack.setEnabled(false)

        scope.launch {
            // try to get the backend supplied TURN/STUN server config
            turn = socket.api.getTurnConfig()
        }
    }

    /**
     * This function sets up a connection to another user (peer).
     * It doesn't start the connection, just sets some parameters up.
     */
    private fun createPeerConnection(userId: String): PeerConnection {
        val iceServers = stunList.map {
            PeerConnection.IceServer.builder(it)
                .setTlsCertPolicy(PeerConnection.TlsCertPolicy.TLS_CERT_POLICY_INSECURE_NO_CHECK)
                .createIceServer()!!
        }.toMutableList()
        val turn = turn
        if (turn != null) {
            val turnConfig = PeerConnection.IceServer.builder(turn.server)
                .setTlsCertPolicy(PeerConnection.TlsCertPolicy.TLS_CERT_POLICY_INSECURE_NO_CHECK)
                .setUsername(turn.username)
                .setPassword(turn.password)
                .createIceServer()!!
            iceServers.add(0, turnConfig)
        }
        val rtcConfig = PeerConnection.RTCConfiguration(iceServers)
        val pcObserver = Observer(userId)
        return factory.createPeerConnection(rtcConfig, pcObserver)!!
    }

    var started = false

    /**
     * Close all peer connections and disconnect from the signaling socket.
     */
    override fun close() {
        socket.close()
        mediaStreams.forEach { stream ->
            stream.audioTracks.forEach {
                it.setEnabled(false)
                stream.removeTrack(it)
            }
        }
        mediaStreams.clear()
        peerConnections.forEach {
            it.value.close()
        }
        peerConnections.clear()
        started = false
    }

    /**
     * Connect to the signaling server.
     */
    fun start() {
        if (!started) {
            socket.start()
            startWebSocket()
            started = true
        }
    }

    /**
     * This controls if our microphone is broadcasted or not.
     */
    var talk
    get() = audioTrack.enabled()
    set(value: Boolean) {
        audioTrack.setEnabled(value)
    }

    /**
     * This function connects to the signaling server and sets up handlers for
     * different types of messages from peers.
     */
    private fun startWebSocket() {
        onStateChange(WebSocketConnecting)
        scope.launch {
            for (ev in socket.observeWebSocketEvent()) {
                if (ev is com.tinder.scarlet.WebSocket.Event.OnConnectionOpened<*>) {
                    socket.sendMessage(Message(type = "ping", userId = socket.api.token!!.data.id, toUserId = null))
                }
            }
        }
        socket.sendMessage(Message(type = "ping", userId = socket.api.token!!.data.id, toUserId = null))
        scope.launch {
            var connected = false
            for(msg in socket.observeMessages()) {
                when (msg.type) {
                    "ping" -> {
                        /*
                         * If we get a ping response (meaning we have successfully connected to the server),
                         * send a Connect message to let others know we're connected.
                         */
                        if (!connected) {
                            socket.sendMessage(
                                Message(
                                    type = "connect",
                                    user = socket.api.token.data.username,
                                    userId = socket.api.token.data.id,
                                    toUserId = null
                                )
                            )
                            onStateChange(WebRTCConnecting)
                            connected = true
                        }
                    }
                    "connect" -> {
                        /*
                         * If we get a Connect response, send and offer to the new peer (call them).
                         */
                        localCandidates.forEach {
                            socket.sendMessage(it)
                        }
                        val peerConnection = createPeerConnection(msg.userId)

                        val mediaStream = factory.createLocalMediaStream(socket.api.token.data.id)
                        mediaStream.addTrack(audioTrack)
                        peerConnection.addStream(mediaStream)
                        mediaStreams += mediaStream

                        peerConnections[msg.userId] = peerConnection

                        // do call
                        val sdpMediaConstraints = MediaConstraints()
                        sdpMediaConstraints.mandatory.add(MediaConstraints.KeyValuePair("OfferToReceiveAudio", "true"))
                        peerConnection.createOffer(object : StubSdpObserver() {
                            override fun onCreateSuccess(desc: SessionDescription?) {
                                peerConnection.setLocalDescription(StubSdpObserver(), desc)
                                socket.sendMessage(
                                    Message(
                                        type = "offer",
                                        sdp = desc!!.description,
                                        userId = socket.api.token.data.id,
                                        toUserId = msg.userId,
                                    )
                                )
                            }
                        }, sdpMediaConstraints)
                    }
                    "offer" -> {
                        /*
                         * If we get an Offer (someone is calling us), answer it with an AudioStream.
                         */
                        if (msg.toUserId != socket.api.token.data.id) {
                            continue
                        }
                        var peerConnection = peerConnections[msg.userId]
                        if (peerConnection == null) {
                            peerConnection = createPeerConnection(msg.userId)
                            peerConnections[msg.userId] = peerConnection
                        }

                        peerConnection.setRemoteDescription(StubSdpObserver(), SessionDescription(SessionDescription.Type.OFFER, msg.sdp))

                        val mediaStream = factory.createLocalMediaStream(socket.api.token.data.id)
                        mediaStream.addTrack(audioTrack)
                        peerConnection.addStream(mediaStream)
                        mediaStreams += mediaStream

                        peerConnection.createAnswer(object : StubSdpObserver() {
                            override fun onCreateSuccess(desc: SessionDescription?) {
                                peerConnection.setLocalDescription(StubSdpObserver(), desc)

                                socket.sendMessage(
                                    Message(
                                        type = "answer",
                                        sdp = desc!!.description,
                                        userId = socket.api.token.data.id,
                                        toUserId = msg.userId,
                                    )
                                )
                            }
                        }, MediaConstraints())
                        onStateChange(WebRTCConnected)
                    }
                    "answer" -> {
                        /*
                         * If we get an Answer, connect to their AudioStream.
                         */
                        if (msg.toUserId != socket.api.token.data.id) {
                            continue
                        }
                        val peerConnection = peerConnections[msg.userId]
                        if (peerConnection == null){
                            println("sus msg")
                            continue
                        }
                        peerConnection.setRemoteDescription(StubSdpObserver(), SessionDescription(SessionDescription.Type.ANSWER, msg.sdp))
                        onStateChange(WebRTCConnected)
                    }
                    "candidate" -> {
                        /*
                         * If we get a Candidate message (someone is changing IPs),
                         * tell the library about it.
                         */
                        if (msg.toUserId != socket.api.token.data.id) {
                            continue
                        }
                        val peerConnection = peerConnections[msg.userId]
                        if (peerConnection == null){
                            println("sus msg")
                            continue
                        }
                        peerConnection.addIceCandidate(IceCandidate(msg.id, msg.label!!, msg.sdp))
                    }
                    else -> {} // This shouldn't happen.
                }
            }
        }
        scope.launch {
            /*
             * Try to detect if our scope has been cancelled,
             * if it is, tear down the connections.
             */
            try {
                delay(Long.MAX_VALUE)
            } catch (_: CancellationException) {
                close()
            }
        }
    }


    private inner class Observer(private val userId: String): StubPeerConnectionObserver() {

        /**
         * If we change IPs, notify others.
         */
        override fun onIceCandidate(candidate: IceCandidate?) {
            candidate!!
            val msg = Message(
                type = "candidate",
                sdp = candidate.sdp,
                label = candidate.sdpMLineIndex,
                id = candidate.sdpMid,
                userId = socket.api.token!!.data.id,
                toUserId = userId,
            )
            localCandidates += msg
            socket.sendMessage(msg)
        }

        /**
         * If streams change, react accordingly.
         */
        override fun onAddStream(stream: MediaStream?) {
            val remoteAudioTrack: AudioTrack = stream!!.audioTracks[0]
            remoteAudioTrack.setEnabled(true)
        }

    }
}
