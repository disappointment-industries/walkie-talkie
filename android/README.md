# Walkie-Talkie alkalmazás

Egy egyszerű és szépen kinéző Walkie-Talkie alkalmazás WebRTC, Jetpack Compose 
és Material You (Material Design 3) használatával.

## WebRTC

Az alkalmazás a hívásokat WebRTC protokollon át bonyolítja le. A WebRTC
signaling-et úgy implementáltam, hogy WebSocket-en át viszem a backend-re az SDP üzeneteket,
melyeket a backend broadcastol a többi fél felé. Ezek után peer-to-peer kapcsolatokat
építenek ki a kliensek egymás közt az SDP üzenetek alapján.

A WebRTC működése dióhéjban:

1. A kliens létrehoz egy PeerConnection-t, amihez hozzáadja a helyi adat streameket (jelen esetben hangot)
2. A kliens készít egy Offer üzenetet, ami egy híváskezdeményezést jelent.
3. A kliens lekéri a STUN szerverektől a publikus IP/port párosát (NAT traversal). (Ezt ICE candidate-nek hívja a STUN és WebRTC)
4. A kliens a kapott IP/port párost hozzáteszi az Offerhez, majd ezt eljuttatja a backendhez.
5. A backend eljuttatja az Offer üzenetet a hívott félhez.
6. A hívott fél is létrehoz egy PeerConnection-t  
7. A hívott elmenti a kapott IP/port párost a PeerConnection-be, mint távoli fél.
8. A hívott lekéri a saját IP/port párosát a STUN szerverektől.
9. A hívott létrehoz egy Answer üzenetet, amibe beleteszi az IP/port párosát, majd elküldi WebSocket-en át.
10. A hívó megkapja az Answer üzenetet, a PeerConnection-be elmenti, mint távoli fél.
11. Ezáltal kiépült a peer-to-peer kapcsolat a két fél között.

A WebRTC megértése és leimplementálás nehézkes volt, mivel a hivatalos WebRTC Android könyvtárhoz
nem találtam friss, értelmes dokumentációt, így a Mozillás JavaScript implementáció dokumentációjából
és Androidos mintaprojektekből értettem meg a protokoll és a könyvtár működését.

## Jetpack Compose

A Jetpack Compose a Google új Androidos UI framework-je. A Jetpack Compose kihasználja a Kotlin
által adott nyelvi eszközöket és a kód preprocesszálását, így XML-ek írása nélkül, Kotlinból építhetünk UI-t deklaratívan.
Ennek előnye, hogy a UI leírása és működése együtt van, könnyen fel lehet fogni a működését.


Ahol például a tipikus megközelítésben valami ilyesmit csinálnánk:

XML UI:

```xml
<SomeComplexLayout>
    ...
    <SomeView
        id="@+id/someView"
    />
    <AnotherView
        id="@+id/anotherView"
    />
    ...
</SomeComplexLayout>
```

Kotlin UI:

```kotlin
if (someCondition) {
    binding.someView.visibility = View.VISIBLE
    binding.anotherView.visibility = View.INVISIBLE
} else {
    binding.someView.visibility = View.INVISIBLE
    binding.anotherView.visibility = View.VISIBLE
}
```

Ott Jetpack Compose-ban a következőt írnánk:

```kotlin
if (someCondition) {
    SomeView()
} else {
    AnotherView()
}
```

Ezáltal a UI-t tömören, teljesen deklaratívan írhatjuk és kikerülhetjük a repetitív kódírásból adódó hibákat,
például ha a felső pédában valahol az `INVISIBLE` helyett `VISIBLE`-t írtünk volna.

## Material You

A Material You a Google Material Design guidline-jainak legújabb verziója. A lényege,
ahonnét a nevét is kapta, hogy a user háttérképe alapján választ színeket és ehhez alkalmazkodik minden alkalmazás.
Emellett a Material 1 és 2-ben lévő elevation rendszert - vagyis azt, hogy árnyékokkal és magasságokkal
mutatják a UI hierarchiáját - lecserélték a tonal elevation rendszerre, ahol színárnyalatokkal hangsúlyozzák
a különböző UI elemeket.

Jetpack Compose-ban szerencsére kifejezetten könnyű volt implementálni a Material You-t és a dinamikus színeket.

## Eltérés a specifikációtól

A specifikációt még azelőtt írtam, hogy a Jetpack Compose-zal jobban megismerkedtem volna, így
mikor elkezdtem leimplementálni az alkalmazásomat rájöttem, hogy a bejelentkező képernyőt ennél az alkalmazásnál
nincs értelme külön Activity-be szervezni, hiszen jobb esetben a user csupán egyszer fogja látni és az alkalmazás megnyitásakor
rossz hatást kelt egy új Activity megnyitása az automatikus bejelentkezés után, illetve ugyanígy szokatlan viselkedés lenne
az, ha az alkalmazás első megnyitáskor azonnal indítana egy másik Activity-t.
Ezért a megoldásom lényegében egy elágazás a UI-ban, valahogy így (nyílván a valóságban kicsit bonyolultabban):

```kotlin
if (loggedIn) {
    ChannelList()
} else {
    LoginScreen()
}
```

